C     Last change:  V    26 Mar 2007   10:50 am
*     E04DGF Example Program Text
	implicit real*8 (a-h,o-z)
*     .. Parameters ..
	INTEGER          MMAX, NMAX, MNMAX
      PARAMETER     (MMAX=128,NMAX=128,MNMAX=2*MMAX*NMAX,mnm=mmax*nmax)
*     .. Local Scalars ..
      DOUBLE PRECISION OBJF
      INTEGER I, IFAIL, ITER, M, N,LA
*     .. Local Arrays ..
      DOUBLE PRECISION OBJGRD(MNMAX),USER(1),WORK(13*MNMAX),X(MNMAX)
      DOUBLE PRECISION x1(mnm)
      INTEGER          IUSER(1), IWORK(MNMAX+1)
*     .. External Subroutines ..
      EXTERNAL  E04DGF, OBJFUN
	common/const/pi,n,m,dn,dm
	common/par/xsi,a1,tau
        COMMON/charge/x1
*     .. Executable Statements ..
        pi=dacos(-1.0d0)
        n=128
        m=128
        n1=n*m
        n11=n1
        dn=dfloat(n)
        dm=dfloat(m)
       write(*,*) '(xsi=L/xi),xsi?'
        read(*,*)xsi
        write(*,*) 'a1?'
        read(*,*)a1
        write(*,*) 'coupling?'
        read(*,*)tau
        WRITE(2,*)'a1',a1,'tau',tau,'xsi',xsi
        xsi=dfloat(n1)/xsi/xsi
        a1=a1*dsqrt(xsi)
        call random_seed()
	do i=1,m
	do j=1,n
	it=(i-1)*n+j
        call random_number(fr)
         call random_number(fr)
        x(it)=6.0d0*(fr-0.5d0)
	enddo
        enddo
        ittt=1
 127    continue
*
*        Solve the problem
*
         IFAIL = -1
*
        CALL E04DGF(N11,OBJFUN,ITER,OBJF,OBJGRD,X,IWORK,WORK,IUSER,USER,
     +               IFAIL)
*
        avpp=0.0d0
        avd=0.0d0
        OPEN(UNIT=9,FILE='dat.9')
	do i=1,m
	do j=1,n
	it=(i-1)*n+j
        avpp=avpp+x(it)
	write(9,*)i,j,x(it),x1(it)
	enddo
	enddo
        CLOSE(UNIT=9)
        avpp=avpp/dn/dm
        WRITE(2,*)'pp',avpp,iter,objf
        WRITE(*,*)'pp',avpp
      STOP
      END
*
      SUBROUTINE OBJFUN(MODE,N1,X,OBJF,OBJGRD,NSTATE,IUSER,USER)
	implicit real*8 (a-h,o-z)
      INTEGER           MODE, N1, NSTATE
      PARAMETER        (MM=128,NM=128,MNM=MM*NM)
      DOUBLE PRECISION  OBJGRD(N1), USER(*), X(N1)
      double precision x1(mnm),y1(mnm),x2(mnm),y2(mnm)
      DOUBLE PRECISION TRIGM(2*MM), TRIGN(2*NM), WORK(2*MNM)
	EXTERNAL C06FUF, C06GCF
      INTEGER           IUSER(*)
	common/const/pi,n,m,dn,dm
	common/par/xsi,a1,tau
        COMMON/par1/dens,xss
        COMMON/charge/x2
	do i=1,mnm
	x1(i)=x(i)
	y1(i)=0.0d0
        x2(i)=x(i)
	y2(i)=0.0d0
	enddo
	ene=0.0d0
c ---- calculation of free energy -----------------------
        IFAIL = 0
       CALL C06FUF(M,N,X1,Y1,'Initial',TRIGM,TRIGN,WORK,IFAIL)
       call ener(x1,y1,m,n,ene)
	energy=ene
      call amult(x1,y1,x2,y2,m,n)
         CALL C06GCF(Y1,M*N,IFAIL)
         CALL C06FUF(M,N,X1,Y1,'Subsequent',TRIGM,TRIGN,WORK,IFAIL)
         CALL C06GCF(Y1,M*N,IFAIL)
         CALL C06GCF(Y2,M*N,IFAIL)
         CALL C06FUF(M,N,X2,Y2,'Subsequent',TRIGM,TRIGN,WORK,IFAIL)
         CALL C06GCF(Y2,M*N,IFAIL)
*
	do i=1,mnm
	energy=energy+(x(i)**2-1.0d0)**2/4.0d0-tau*x(i)
	enddo
      OBJF = energy
*
      IF (MODE.EQ.2) THEN
*        -- Compute inverse transform
*
	do i=1,mnm
      OBJGRD(i)=x1(i)+x(i)*(x(i)**2-1.0d0)-tau
c	OBJGRD(i)=x1(i)
	enddo
      END IF
*
      RETURN
      END
*
      SUBROUTINE amult(x1,y1,x2,y2,n1,n2)
	implicit real*8 (a-h,o-z)
*     .. Scalar Arguments ..
      INTEGER           N1, N2
*     .. Array Arguments ..
      DOUBLE PRECISION  X1(N1,N2), Y1(N1,N2), X2(N1,N2), Y2(N1,N2)
*     .. Local Scalars ..
      INTEGER           I, J
	common/par/xsi,a1,tau
*     .. Executable Statements ..
	pi=dacos(-1.0d0)
	dn1=dfloat(n1)
	dn2=dfloat(n2)
      	do i=1,n1
	do j=1,n2
	zx=2.0d0*pi*dfloat(i-1)/dn1
	zy=2.0d0*pi*dfloat(j-1)/dn2
	ek=-dcos(zx)-dcos(zy)+2.0d0
        x2(i,j)=x1(i,j)*2.0d0*dsqrt(xsi*ek)
	y2(i,j)=y1(i,j)*2.0d0*dsqrt(xsi*ek)
	x1(i,j)=x1(i,j)*4.0d0*(xsi*ek-a1*dsqrt(ek))
	y1(i,j)=y1(i,j)*4.0d0*(xsi*ek-a1*dsqrt(ek))
	enddo
	enddo
      RETURN
      END
*
      SUBROUTINE ener(x1,y1,n1,n2,ene)
*     .. Scalar Arguments ..
	implicit real*8 (a-h,o-z)
      INTEGER           N1, N2
*     .. Array Arguments ..
      DOUBLE PRECISION  X1(N1,N2), Y1(N1,N2)
*     .. Local Scalars ..
      INTEGER           I, J
	common/par/xsi,a1,tau
*     .. Executable Statements ..
	pi=dacos(-1.0d0)
	ene=0.0d0
	dn1=dfloat(n1)
	dn2=dfloat(n2)
      	do i=1,n1
	do j=1,n2
	zx=2.0d0*pi*dfloat(i-1)/dn1
	zy=2.0d0*pi*dfloat(j-1)/dn2
	ek=-dcos(zx)-dcos(zy)+2.0d0
        ene=ene+2.0d0*(xsi*ek-a1*dsqrt(ek))*(x1(i,j)**2+y1(i,j)**2)
	enddo
	enddo
      RETURN
      END
*
