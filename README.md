# Electron phase sepration in 2D systems

Solution of a partial differential equation (PDE) derived from  Landau formalism of phase transitions to describe electron phase separation in layered Perovskites. The obtained PDE contains short and long-range dependence of spatial coordinates, and it is solved by finite element meshing where the long-range interaction is transformed with the assistance of a Fourier transformation. 

